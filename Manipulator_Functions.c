#define LLIFT_SENSOR_INDEX LLiftEncoder
#define RLIFT_SENSOR_INDEX RLiftEncoder
#define LIFT_SENSOR_SCALE 1
#define LLIFT_MOTOR_INDEX LLift
#define RLIFT_MOTOR_INDEX RLift
#define LIFT_MOTOR_SCALE -1
#define LIFT_DRIVE_MAX 127
#define LIFT_DRIVE_MIN (-127)
#define LIFT_INTEGRAL_LIMIT 50
float pid_Kp = 4.0;
float pid_Ki = 0;
float pid_Kd = 0.00;
int lift_g;
int lift_goal;

task Lift_Function()
{
	float LpidSensorCurrentValue;
	float LpidError;
	float LpidLastError;
	float LpidIntegral;
	float LpidDerivative;
	float LpidDrive;
	float RpidSensorCurrentValue;
	float RpidError;
	float RpidLastError;
	float RpidIntegral;
	float RpidDerivative;
	float RpidDrive;

	// Init the variables - thanks Glenn :)
	LpidLastError = 0;
	LpidIntegral = 0;
	RpidLastError = 0;
	RpidIntegral = 0;

	while(true)
	{
		// Read the sensor value and scale
		LpidSensorCurrentValue = SensorValue[LLIFT_SENSOR_INDEX] * LIFT_SENSOR_SCALE;

		// calculate error
		LpidError = LpidSensorCurrentValue - lift_goal;

		// integral - if Ki is not 0
		if(pid_Ki != 0)
		{
			// If we are inside controlable window then integrate the error
			if(abs(LpidError) < LIFT_INTEGRAL_LIMIT)
				LpidIntegral = LpidIntegral + LpidError;
			else
				LpidIntegral = 0;
		}
		else
			LpidIntegral = 0;

		// calculate the derivative
		LpidDerivative = LpidError - LpidLastError;
		LpidLastError  = LpidError;

		// calculate drive
		LpidDrive = (pid_Kp * LpidError) + (pid_Ki * LpidIntegral) + (pid_Kd * LpidDerivative);

		// limit drive
		if(LpidDrive > LIFT_DRIVE_MAX)
			LpidDrive = LIFT_DRIVE_MAX;
		if(LpidDrive < LIFT_DRIVE_MIN)
			LpidDrive = LIFT_DRIVE_MIN;

		// send to motor
		LLIFT_MOTOR_INDEX = LpidDrive * LIFT_MOTOR_SCALE;

		// Read the sensor value and scale
		RpidSensorCurrentValue = SensorValue[RLIFT_SENSOR_INDEX] * LIFT_SENSOR_SCALE;

		// calculate error
		RpidError = RpidSensorCurrentValue - lift_goal;

		// integral - if Ki is not 0
		if(pid_Ki != 0)
		{
			// If we are inside controlable window then integrate the error
			if(abs(RpidError) < LIFT_INTEGRAL_LIMIT)
				RpidIntegral = RpidIntegral + RpidError;
			else
				RpidIntegral = 0;
		}
		else
			RpidIntegral = 0;

		// calculate the derivative
		RpidDerivative = RpidError - RpidLastError;
		RpidLastError  = RpidError;

		// calculate drive
		RpidDrive = (pid_Kp * RpidError) + (pid_Ki * RpidIntegral) + (pid_Kd * RpidDerivative);

		// limit drive
		if(RpidDrive > LIFT_DRIVE_MAX)
			RpidDrive = LIFT_DRIVE_MAX;
		if(RpidDrive < LIFT_DRIVE_MIN)
			RpidDrive = LIFT_DRIVE_MIN;

		// send to motor
		RLIFT_MOTOR_INDEX = RpidDrive * LIFT_MOTOR_SCALE;

		// Run at 50Hz
		wait1Msec(25);
	}
}

task GetPreset() //Presets
{
	while(true)
	{
		if(lift_g == 0)
		{
			lift_goal = 0;
		}
		else if(lift_g == 1)
		{
			lift_goal = 115;
		}
		else if(lift_g == 2)
		{
			lift_goal = 175;
		}
		else if(lift_g == 3)
		{
			lift_goal = 80;
		}
	}
}

task arm()
{
	StartTask(GetPreset);
	StartTask(Lift_Function);
	while(true)
	{
		if(vexRT[Btn6U] == 1 && lift_g == 0)
		{
			lift_g = 1;
			LiftwaitForRelease();
		}
		else if(vexRT[Btn6U] == 1 && lift_g == 1)
		{
			lift_g = 2;
			LiftwaitForRelease();
		}
		else if(vexRT[Btn6D] == 1)
		{
			lift_g = 0;
			LiftwaitForRelease();
		}
		else if(vexRT[Btn8D] == 1)
		{
			lift_g = 3;
			LiftwaitForRelease();
		}
	}
}
