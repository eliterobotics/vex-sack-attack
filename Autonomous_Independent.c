int last_arm_pos;
bool next = false;
bool rep = true;

task GetAutonomous()
{
	while(true)
	{
		switch(AtnmsSlctn){
		case 1:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn = 9;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 2:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 3:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 4:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 5:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 6:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 7:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 8:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn++;
			}
			break;
		case 9:
			LCDwaitForPress();
			if(nLCDButtons == leftButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn--;
			}
			else if(nLCDButtons == rightButton)
			{
				LCDwaitForRelease();
				AtnmsSlctn = 1;
			}
			break;
		default:
			AtnmsSlctn = 1;
			break;
		}
	}
}

task AutonomousIndependent()
{
	SensorValue[LEncdr] = 0;
	SensorValue[REncdr] = 0;
	SensorValue[RLiftEncoder] = 0;
	SensorValue[LLiftEncoder] = 0;
	if(AtnmsSlctn == 1) // Score floor only
	{
		Intake = 127;
		wait1Msec(1000);
		LDrv = -127;
		RDrv = -127;
		wait1Msec(1000);
		LDrv = 0;
		RDrv = 0;
	}
	else if(AtnmsSlctn == 2) //Left. Collect trough gold, collect pyramid, score trough
	{
		LDR = 1000;
		RDR = 1000;
		StartTask(D_Function);
		wait1Msec(1400);
		Intake = 127;
		wait1Msec(700);
		LDR = 1500;
		RDR = 1500;
		wait1Msec(4000);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = -1500;
		RDR = -1500;
		wait1Msec(1500);
		Intake = 0;
		untilTouch(Trigger);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		lift_goal = 500;
		StartTask(Lift_Function);
		LDR = 1300;
		RDR = 1300;
		wait1Msec(1900);
		Intake = -127;
		PlaySoundFile("WindowsError.wav");
		wait1Msec(500);
		PlaySoundFile("WindowsFail.wav");
		wait1Msec(520);
		PlaySoundFile("WindowsRemove.wav");
		last_arm_pos = lift_goal;
	}
	else if(AtnmsSlctn == 3) // Left. Collect pyramid, collect trough gold, score trough. (Not tested)
	{
		LDR = 400;
		RDR = 400;
		StartTask(D_Function);
		Intake = 127;
		wait1Msec(800);
		LDR = 800;
		RDR = 800;
		wait1Msec(300);
		LDR = -400;
		RDR = -400;
		untilTouch(Trigger);
		StopTask(D_Function);
		Intake = 0;
		while(SensorValue[Trigger] == 1){}
		wait1Msec(5);
		untilTouch(Trigger);
		StartTask(D_Function);
		LDR = 1300;
		RDR = 1300;
		wait1Msec(700);
		Intake = 127;
		wait1Msec(700);
		Intake = 0;
		LDR = 800;
		RDR = 800;
		wait1Msec(500);
		LDR = 1300;
		RDR = 1300;
		wait1Msec(200);
		Intake = -127;
		last_arm_pos = lift_goal;
	}
	else if(AtnmsSlctn == 4) // Right. Collect trough gold, collect floor gold, score high
	{
		LDR = 1000;
		RDR = 1000;
		StartTask(D_Function);
		wait1Msec(1800);
		Intake = 127;
		wait1Msec(700);
		LDR = 1400;
		RDR = 1400;
		wait1Msec(800);
		LDR = 1400;
		RDR = 1400;
		wait1Msec(300);
		LDR = 0;
		RDR = 0;
		wait1Msec(500);
		Intake = 0;
		untilTouch(Trigger);
		StopTask(D_Function);
		LDrv = 0;
		RDrv = 0;
		while(SensorValue[Trigger] != 0){}
		wait1Msec(5);
		untilTouch(Trigger);
		lift_goal = 20; //FIX THIS
		StartTask(Lift_Function);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		StartTask(D_Function);
		LDR = 1380;
		RDR = 1380;
		wait1Msec(600);
		Intake = 127;
		wait1Msec(600);
		lift_goal = 0;
		wait1Msec(1800);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = 170;
		RDR = -170;
		lift_goal = 850;
		wait1Msec(600);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = -110;
		RDR = -110;
		wait1Msec(700);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = 640;
		RDR = 640;
		wait1Msec(350);
		Intake = -127;
		wait1Msec(2000);
		LDR = 0;
		RDR = 0;
		last_arm_pos = lift_goal;
	}
	else if(AtnmsSlctn == 5) // Left. Collect trough gold, collect floor gold, score high
	{
		LDR = 1000;
		RDR = 1000;
		StartTask(D_Function);
		wait1Msec(1800);
		Intake = 127;
		wait1Msec(700);
		LDR = 1400;
		RDR = 1400;
		wait1Msec(800);
		LDR = 1400;
		RDR = 1400;
		wait1Msec(300);
		LDR = 0;
		RDR = 0;
		wait1Msec(500);
		Intake = 0;
		untilTouch(Trigger);
		StopTask(D_Function);
		LDrv = 0;
		RDrv = 0;
		while(SensorValue[Trigger] != 0){}
		wait1Msec(5);
		untilTouch(Trigger);
		lift_goal = 20; // FIX THIS
		StartTask(Lift_Function);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		StartTask(D_Function);
		LDR = 1380;
		RDR = 1380;
		wait1Msec(600);
		Intake = 127;
		wait1Msec(600);
		lift_goal = 0;
		wait1Msec(1800);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = -170;
		RDR = 170;
		lift_goal = 850;
		wait1Msec(600);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = -110;
		RDR = -110;
		wait1Msec(700);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = 640;
		RDR = 640;
		wait1Msec(350);
		Intake = -127;
		wait1Msec(2000);
		LDR = 0;
		RDR = 0;
		last_arm_pos = lift_goal;
	}
	else if(AtnmsSlctn == 6)
	{
		//code
	}
	else if(AtnmsSlctn == 7)
	{
		//code
	}
	else if(AtnmsSlctn == 8)
	{
		//code
	}
	else if(AtnmsSlctn == 9) // Programming Skills
	{
		LDR = 1000;
		RDR = 1000;
		StartTask(D_Function);
		wait1Msec(1800);
		Intake = 127;
		wait1Msec(700);
		LDR = 1400;
		RDR = 1400;
		wait1Msec(800);
		LDR = 1400;
		RDR = 1400;
		wait1Msec(300);
		LDR = 0;
		RDR = 0;
		wait1Msec(500);
		Intake = 0;
		while(SensorValue[Trigger] != 1){}
		StopTask(D_Function);
		LDrv = 0;
		RDrv = 0;
		wait1Msec(500);
		while(SensorValue[Trigger] != 0){}
		wait1Msec(5);
		untilTouch(Trigger);
		lift_goal = 20; //FIX THIS
		StartTask(Lift_Function);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		StartTask(D_Function);
		LDR = 1380;
		RDR = 1380;
		wait1Msec(600);
		Intake = 127;
		wait1Msec(600);
		lift_goal = 0;
		wait1Msec(1800);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = 170;
		RDR = -170;
		lift_goal = 850;
		wait1Msec(600);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = -110;
		RDR = -110;
		wait1Msec(700);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = 640;
		RDR = 640;
		wait1Msec(350);
		Intake = -127;
		wait1Msec(2000);
		LDR = 0;
		RDR = 0;
		wait1Msec(2000);
		Intake = 0;
		LDR = 170;
		RDR = -170;
		lift_goal = 0;
		wait1Msec(600);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = -700;
		RDR = -700;
		while(SensorValue[Trigger] != 1){}
		StopTask(D_Function);
		LDrv = 0;
		RDrv = 0;
		wait1Msec(200);
		while(SensorValue[Trigger] != 0){}
		wait1Msec(5);
		untilTouch(Trigger);
		while(next == false)
		{
			rep = true;
			LDR = 600;
      RDR = 600;
      StartTask(D_Function);
      wait1Msec(800);
      Intake = 127;
      wait1Msec(2500);
      LDR = 0;
      RDR = 0;
      while(next == false && rep == true)
      {
      	if(SensorValue[Repeat] != 0)
      	{
      		rep = false;
      	}
      	else if(SensorValue[Trigger] != 0)
      	{
      		next = true;
      	}
      }
		}
		Intake = 0;
		StopTask(D_Function);
		wait1Msec(250);
		untilTouch(Trigger);
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;
		LDR = 1400;
		RDR = 1400;
		StartTask(D_Function);
		wait1Msec(1800);
		Intake = 127;
		wait1Msec(1500);
		LDR = 800;
		RDR = 800;
		wait1Msec(400);
		StartTask(Lift_Function);
		lift_goal = 500;
		wait1Msec(800);
		LDR = 1300;
		RDR = 1300;
		wait1Msec(800);
		Intake = -127;
		last_arm_pos = lift_goal;
	}
}
