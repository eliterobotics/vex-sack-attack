void allMotorsOff();
void allTasksStop();
void pre_auton();
task autonomous();
task usercontrol();

const short leftButton = 1;
const short centerButton = 2;
const short rightButton = 4;
int AtnmsSlctn = 1;
int LLift = 0;
int RLift = 0;
int LDrv = 0;
int RDrv = 0;
int Intake = 0;
bool bStopTasksBetweenModes = true;

task Display()
{
	string mainBattery, backupBattery;
	while(true)
	{
		if(nLCDButtons != centerButton && bIfiRobotDisabled == true)
		{
			if(AtnmsSlctn == 1)
			{
				displayLCDCenteredString(0, "A1 LR: SF");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 2)
			{
				displayLCDCenteredString(0, "A2 L: CTG,CP,ST");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 3)
			{
				displayLCDCenteredString(0, "A3 L: CP,CTG,ST");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 4)
			{
				displayLCDCenteredString(0, "A4 R: CTG,CFG,SH");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 5)
			{
				displayLCDCenteredString(0, "A5 L: CTG,CFG,SH");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 6)
			{
				displayLCDCenteredString(0, "Autonomous 6");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 7)
			{
				displayLCDCenteredString(0, "Autonomous 7");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 8)
			{
				displayLCDCenteredString(0, "Autonomous 8");
				displayLCDCenteredString(1, "<     Batt     >");
			}
			else if(AtnmsSlctn == 9)
			{
				displayLCDCenteredString(0, "ProgrammingSkill");
				displayLCDCenteredString(1, "<     Batt     >");
			}
		}
		else if(nLCDButtons == centerButton || bIfiRobotDisabled == false)
		{
			displayLCDString(0, 0, "Primary: ");
			sprintf(mainBattery, "%1.2f%c  ", nImmediateBatteryLevel/1000.0,'V');
			displayNextLCDString(mainBattery);

			displayLCDString(1, 0, "Secondary: ");
			sprintf(backupBattery, "%1.2f%c", SecondBattery/70.0, 'V');
			displayNextLCDString(backupBattery);
		}
		else
		{
			clearLCDLine(0);
			clearLCDLine(1);
		}
	}
}

task Mapper()
{
	while(true)
	{
		motor[LFDrv] = motor[LBDrv] = LDrv;
		motor[RFDrv] = motor[RBDrv] = RDrv;
		motor[LeftLift1] = motor[LeftLift2] = LLift;
		motor[RightLift1] = motor[RightLift2] = RLift;
		motor[IntakeL] = motor[IntakeR] = Intake;
	}
}

task main() //MAIN
{
	bLCDBacklight = true;
	// Master CPU will not let competition start until powered on for at least 2-seconds
	float time = 0;

	while(time < 3)
	{
		if(time == 0 || time == .75 || time == 1.5)
		{
			displayLCDPos(0, 0);
			displayNextLCDString("Startup");
		}
		else if(time == 0.25 || time == 1 || time == 1.75)
		{
			displayLCDPos(0, 0);
			displayNextLCDString("Startup.");
		}
		else if(time == .5 || time == 1.25)
		{
			displayLCDPos(0, 0);
			displayNextLCDString("Startup..");
		}
		else if(time >= 2)
		{
			displayLCDPos(0, 0);
			displayNextLCDString("COMPLETE! :D");
		}

		wait1Msec(250);
		time = time + .25;
		clearLCDLine(0);
		clearLCDLine(1);
	}

	while (true) //START
	{
		SensorValue[LEncdr] = 0;
		SensorValue[REncdr] = 0;

		while (bIfiRobotDisabled)
		{
		pre_auton();
			StartTask(Display);
			while (true)
			{
				if (!bIfiRobotDisabled)
					break;
				wait1Msec(25);

				if (!bIfiRobotDisabled)
					break;
				wait1Msec(25);

				if (!bIfiRobotDisabled)
					break;
				wait1Msec(25);

				if (!bIfiRobotDisabled)
					break;
				wait1Msec(25);
			}
		}

		if (bIfiAutonomousMode)
		{
			StartTask(autonomous);
			StartTask(Mapper);
			StartTask(Display);

			// Waiting for autonomous phase to end
			while (bIfiAutonomousMode && !bIfiRobotDisabled)
			{
				if (!bVEXNETActive)
				{
					if (nVexRCReceiveState == vrNoXmiters) // the transmitters are powered off!!
						allMotorsOff();
				}
				wait1Msec(25);               // Waiting for autonomous phase to end
			}
			allMotorsOff();
			if(bStopTasksBetweenModes)
			{
				allTasksStop();
			}
		}

		else
		{
			StartTask(usercontrol);
			StartTask(Mapper);
			StartTask(Display);

			// Here we repeat loop waiting for user control to end and (optionally) start
			// of a new competition run
			while (!bIfiAutonomousMode && !bIfiRobotDisabled)
			{
				if (nVexRCReceiveState == vrNoXmiters) // the transmitters are powered off!!
					allMotorsOff();
				wait1Msec(25);
			}
			allMotorsOff();
			if(bStopTasksBetweenModes)
			{
				allTasksStop();
			}
		}
	}
}

void allMotorsOff()
{
	motor[port1] = 0;
	motor[port2] = 0;
	motor[port3] = 0;
	motor[port4] = 0;
	motor[port5] = 0;
	motor[port6] = 0;
	motor[port7] = 0;
	motor[port8] = 0;
#if defined(VEX2)
	motor[port9] = 0;
	motor[port10] = 0;
#endif
}

void allTasksStop()
{
	StopTask(1);
	StopTask(2);
	StopTask(3);
	StopTask(4);
#if defined(VEX2)
	StopTask(5);
	StopTask(6);
	StopTask(7);
	StopTask(8);
	StopTask(9);
	StopTask(10);
	StopTask(11);
	StopTask(12);
	StopTask(13);
	StopTask(14);
	StopTask(15);
	StopTask(16);
	StopTask(17);
	StopTask(18);
	StopTask(19);
#endif
}

void LCDwaitForPress()
{
	while(nLCDButtons == 0){}
	wait1Msec(5);
}

void LCDwaitForRelease()
{
	while(nLCDButtons != 0){}
	wait1Msec(5);
}

void LiftwaitForRelease()
{
	while(vexRT[Btn6U] != 0 || vexRT[Btn6D] != 0){}
	wait1Msec(5);
}
