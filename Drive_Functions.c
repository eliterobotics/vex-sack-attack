#define LD_SENSOR_INDEX LEncdr
#define RD_SENSOR_INDEX REncdr
#define LD_MOTOR_INDEX LDrv
#define RD_MOTOR_INDEX RDrv
#define D_SENSOR_SCALE 1
#define D_MOTOR_SCALE -1
#define D_DRIVE_MAX 80
#define D_DRIVE_MIN (-80)
#define D_INTEGRAL_LIMIT 50
float pid_KpD = 0.5;
float pid_KiD = 0.0;
float pid_KdD = 0.001;
int LDR;
int RDR;

task D_Function()
{
	SensorValue[LEncdr] = 0;
	SensorValue[REncdr] = 0;
	float LpidSensorCurrentValue;
	float LpidError;
	float LpidLastError;
	float LpidIntegral;
	float LpidDerivative;
	float LpidDrive;
	float RpidSensorCurrentValue;
	float RpidError;
	float RpidLastError;
	float RpidIntegral;
	float RpidDerivative;
	float RpidDrive;

	// Init the variables - thanks Glenn :)
	LpidLastError = 0;
	LpidIntegral = 0;
	RpidLastError = 0;
	RpidIntegral = 0;

	while(true)
	{
		// Read the sensor value and scale
		LpidSensorCurrentValue = SensorValue[LD_SENSOR_INDEX] * D_SENSOR_SCALE;

		// calculate error
		LpidError = LpidSensorCurrentValue - LDR;

		// integral - if Ki is not 0
		if(pid_KiD != 0)
		{
			// If we are inside controlable window then integrate the error
			if(abs(LpidError) < D_INTEGRAL_LIMIT)
				LpidIntegral = LpidIntegral + LpidError;
			else
				LpidIntegral = 0;
		}
		else
			LpidIntegral = 0;

		// calculate the derivitive
		LpidDerivative = LpidError - LpidLastError;
		LpidLastError  = LpidError;

		// calculate drive
		LpidDrive = (pid_KpD * LpidError) + (pid_KiD * LpidIntegral) + (pid_KdD * LpidDerivative);

		// limit drive
		if(LpidDrive > D_DRIVE_MAX)
			LpidDrive = D_DRIVE_MAX;
		if(LpidDrive < D_DRIVE_MIN)
			LpidDrive = D_DRIVE_MIN;

		// send to motor
		LD_MOTOR_INDEX = LpidDrive * D_MOTOR_SCALE;

		// Read the sensor value and scale
		RpidSensorCurrentValue = SensorValue[RD_SENSOR_INDEX] * D_SENSOR_SCALE;

		// calculate error
		RpidError = RpidSensorCurrentValue - RDR;

		// integral - if Ki is not 0
		if(pid_KiD != 0)
		{
			// If we are inside controlable window then integrate the error
			if(abs(RpidError) < D_INTEGRAL_LIMIT)
				RpidIntegral = RpidIntegral + RpidError;
			else
				RpidIntegral = 0;
		}
		else
			RpidIntegral = 0;

		// calculate the derivative
		RpidDerivative = RpidError - RpidLastError;
		RpidLastError  = RpidError;

		// calculate drive
		RpidDrive = (pid_KpD * RpidError) + (pid_KiD * RpidIntegral) + (pid_KdD * RpidDerivative);

		// limit drive
		if(RpidDrive > D_DRIVE_MAX)
			RpidDrive = D_DRIVE_MAX;
		if(RpidDrive < D_DRIVE_MIN)
			RpidDrive = D_DRIVE_MIN;

		// send to motor
		RD_MOTOR_INDEX = RpidDrive * D_MOTOR_SCALE;

		// Run at 50Hz
		wait1Msec(25);
	}
}
